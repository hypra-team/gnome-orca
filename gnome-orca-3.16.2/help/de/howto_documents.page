<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="howto_documents" xml:lang="de">
  <info>
    <link type="guide" xref="index#reading"/>
    <link type="next" xref="howto_text_attributes"/>
    <title type="sort">1. Dokumente</title>
    <desc>Inhalte lesen</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>
  <title>Dokumente</title>
  <p>
    To read the contents of any document, use the application's built-in caret
    navigation mode. As you navigate within the text of the document, <app>Orca</app>
    will present your new location. As a result, you are likely already familiar with
    how to read a document using <app>Orca</app>. For instance:
  </p>
  <list>
    <item>
      <p>
        Use <key>Left</key> and <key>Right</key> to move and read by character.
      </p>
    </item>
    <item>
      <p>Verwenden Sie <keyseq><key>Strg</key><key>Links</key></keyseq> und <keyseq><key>Strg</key><key>Rechts</key></keyseq>, um wortweise zu lesen und zu navigieren.</p>
    </item>
    <item>
      <p>Verwenden Sie <key>Auf</key> und <key>Ab</key>, um zeilenweise vorzulesen und zu navigieren.</p>
    </item>
    <item>
      <p>
        Use <key>Shift</key> in combination with the above commands to select and
        unselect text.
      </p>
    </item>
  </list>
  <note style="tip">
    <title>Enabling Caret Navigation in an Application</title>
    <p>
      Not all applications have caret navigation enabled by default. For many GNOME
      applications, caret navigation can be toggled on and off by pressing <key>F7</key>.
    </p>
  </note>
  <p>
    In addition to reading a document by caret navigation, you may find it helpful
    to read, spell, and obtain the Unicode value for the current text. You can do
    these things through <app>Orca</app>'s <link xref="howto_flat_review">Flat
    Review feature</link>.
  </p>
  <p>
    Finally, in order to have <app>Orca</app> speak the entire document from your
    present location, use the SayAll command. It, along with a more complete
    listing of <app>Orca</app>'s commands for accessing document text, can
    be found in the <link xref="commands_reading">Reading Commands</link> guide.
  </p>
</page>
