<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="commands_where_am_i" xml:lang="de">
  <info>
    <link type="next" xref="commands_time_date_notifications"/>
    <link type="guide" xref="commands#getting_started"/>
    <link type="seealso" xref="howto_whereami"/>
    <title type="sort">3. Wo bin ich</title>
    <title type="link">Wo bin ich</title>
    <desc>Befehle, um Ihren Standort zu erfahren</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>
  <title>Befehle zu »Wo bin ich«</title>
  <p>
    <app>Orca</app>'s Where Am I feature gives you context-sensitive details
    about your present location. For instance, in tables, Where Am I will give
    you details about the table cell you are in, but in text it will present
    the current line along with any text which happens to be selected. The full
    list of what you can expect <app>Orca</app> to present can be found in the
    <link xref="howto_whereami">Introduction to Where Am I</link>.
  </p>
  <p><app>Orca</app> provides the following Where Am I commands:</p>
  <list>
    <item>
      <p>Die einfache »Wo bin ich«-Funktion ausführen:</p>
      <list>
        <item>
          <p>Desktop: <key>Eingabetaste (Nmblck)</key></p>
        </item>
        <item>
          <p>Laptop: <keyseq><key>Orca-Zusatztaste</key><key>Eingabetaste</key></keyseq></p>
        </item>
      </list>
    </item>
    <item>
      <p>Die detaillierte »Wo bin ich«-Funktion ausführen:</p>
      <list>
        <item>
          <p>Desktop: <key>Eingabetaste (Nmblck)</key> (Doppelklick)</p>
        </item>
        <item>
          <p>Laptop: <keyseq><key>Orca-Zusatztaste</key><key>Eingabetaste</key></keyseq> (Doppelklick)</p>
        </item>
      </list>
    </item>
  </list>
  <p>
    In addition to the dedicated Where Am I commands, <app>Orca</app> has
    two additional commands related to obtaining your present location:
  </p>
  <list>
    <item>
      <p>Die Titelleiste anzeigen:</p>
      <list>
        <item>
          <p>Desktop: <keyseq><key>Orca-Zusatztaste</key><key>Eingabetaste (Nmblck)</key></keyseq></p>
        </item>
        <item>
          <p>Laptop: <keyseq><key>Orca-Zusatztaste</key><key>Schrägstrick</key></keyseq></p>
        </item>
      </list>
    </item>
    <item>
      <p>Die Statusleiste anzeigen:</p>
      <list>
        <item>
          <p>Desktop: <keyseq><key>Orca-Zusatztaste</key><key>Eingabetaste (Nmblck)</key></keyseq> (Doppelklick)</p>
        </item>
        <item>
          <p>Laptop: <keyseq><key>Orca-Zusatztaste</key><key>Schrägstrich</key></keyseq> (Doppelklick)</p>
        </item>
      </list>
    </item>
  </list>
</page>
