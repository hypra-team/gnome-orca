<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_braille" xml:lang="de">
  <info>
    <title type="sort">3. Braille</title>
    <title type="link">Braille</title>
    <desc>Einstellungen der <app>Orca</app>-Unterstützung für Braillezeilen</desc>
    <link type="guide" xref="preferences#orca"/>
    <link type="next" xref="preferences_key_echo"/>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>
  <title>Braille-Einstellungen</title>
  <section id="braillesupport">
    <title>Braille-Unterstützung aktivieren</title>
    <p>
      This check box toggles whether or not <app>Orca</app> will make
      use of a braille display. If BrlTTY is not running, <app>Orca</app>
      will recover gracefully and will not communicate with the braille
      display.
    </p>
    <p>Standardwert: nicht aktiviert</p>
    <note style="tip">
      <p>
        If you configure BrlTTY later on, you need to restart <app>Orca</app>
        in order to use braille.
      </p>
    </note>
  </section>
  <section id="contractedbraille">
    <title>Blindenkurzschrift aktivieren</title>
    <p>
      Orca supports contracted braille via the liblouis project. Because many
      distros include liblouis, you will likely automatically have access to
      contracted braille support in <app>Orca</app>.
    </p>
    <p>
      To enable contracted braille on a system where liblouis has been installed,
      be sure that the <gui>Enable Contracted Braille</gui> checkbox is checked.
      Then choose your desired translation table from the <gui>Contraction
      Table</gui> combo box.
    </p>
    <p>Standardwert: nicht aktiviert</p>
  </section>
  <section id="rolenames">
    <title>Abgekürzte Rollennamen</title>
    <p>
      This check box determines the manner in which role names are
      displayed and can be used to help conserve real estate on the
      braille display. For instance, if a slider had focus, the word
      "slider" would be displayed if abbreviated role names is not
      checked; if it were checked, "sldr" would be displayed instead.
    </p>
    <p>Standardwert: nicht aktiviert</p>
  </section>
  <section id="eolindicator">
    <title>Zeilenende-Symbol nicht anzeigen</title>
    <p>
      Checking this checkbox tells <app>Orca</app> to not present the
      "$l" string at the end of a line of text.
    </p>
    <p>Standardwert: nicht aktiviert</p>
  </section>
  <section id="verbosity">
    <title>Ausführlichkeit</title>
    <p>Diese Gruppe Auswahlknöpfe legt fest, welche Informationen in verschiedenen Situationen auf der Braille-Zeile ausgegeben werden sollen. Zum Beispiel werden bei der Einstellung »Ausführlich« Tastenkombinationen und Rolleninformationen ausgegeben, während diese bei der Auswahl von »Kurz« nicht ausgegeben werden.</p>
    <p>Vorgabewert: <gui>Ausführlich</gui></p>
  </section>
  <section id="selectionandhyperlink">
    <title>Indikatoren für Auswahlen und Hyperlinks</title>
    <p>
      The <gui>Selection Indicator</gui> and <gui>Hyperlink Indicator</gui>
      radio button groups allow you to configure <app>Orca</app>'s behavior
      when displaying selected text and hyperlinks. By default, when you
      encounter either, <app>Orca</app> will "underline" that text on
      your braille display with Dots 7 and 8. If you would prefer, you
      can change the indicator to only be Dot 7, only be Dot 8, or not
      be present at all.
    </p>
    <p>Vorgabewert: <gui>Punkte 7 und 8</gui></p>
    <note style="tip">
      <title>Indikatoren für Textattribute</title>
      <p>
        You can also optionally have text attributes indicated in braille.
        Enabling this feature and choosing which attributes are of
        interest is done on the <link xref="preferences_text_attributes">
        <gui>Text Attributes</gui> page</link> of the preferences dialog.
      </p>
    </note>
  </section>
</page>
