<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_general" xml:lang="cs">
  <info>
    <title type="sort">1. Obecné</title>
    <title type="link">Obecné</title>
    <desc>Jak nastavit základní chování čtečky <app>Orca</app></desc>
    <link type="guide" xref="preferences#orca"/>
    <link type="next" xref="preferences_voice"/>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  </info>
  <title>Obecné předvolby</title>
  <section id="keyboardlayout">
    <info>
      <desc>Volba v dialogovém okně s předvolbami, která slouží k výběru rozložení klávesnice (stolní počítač nebo přenosný počíta), které má používat čtečka <app>Orca</app></desc>
    </info>
    <title>Rozložení klávesnice</title>
    <p>Pomocí skupinových přepínačů <gui>rozložení klávesnice</gui> se určuje, jestli budete pracovat na klávesnici stolního počítače (tj. s numerickou částí klávesnice) nebo přenosného počítače. Výběr rozložení klávesnice ovlivní <key>Modifikátor Orca</key> a počet klávesových zkratek pro provádění příkazů <app>Orca</app>.</p>
    <p>Výchozí hodnota: <gui>Stolní počítač</gui></p>
  </section>
  <section id="presenttooltips">
    <info>
      <desc>Volba v dialogovém okně s předvolbami, která povoluje prezentování vysvětlivek, které se objeví jako následek najetí ukazatelem myši.</desc>
    </info>
    <title>Prezentovat vysvětlivky</title>
    <p>Když je zaškrtnuto, říká tato volba čtečce <app>Orca</app>, že má prezentovat informace o vysvětlivkách, pokud se objeví jako následek najetí myší. Specifické způsoby vyvolání vysvětlivek, jako je například zmáčknutí <keyseq><key>Ctrl</key> <key>F1</key></keyseq> ve chvíli, kdy je objekt zaměřen, způsobí prezentování vysvětlivek vždy, bez ohledu na toto nastavení.</p>
    <p>Výchozí hodnota: nezaškrtnuto</p>
  </section>
  <section id="objectundermouse">
    <info>
      <desc>Volba v dialogovém okně s předvolbami, která povoluje prezentování objektu pod ukazatelem myši.</desc>
    </info>
    <title>Oznamovat objekt pod ukazatelem</title>
    <p>Když je zaškrtnuto, řídká tato volba čtečce <app>Orca</app>, že má prezentovat informace o objektu pod ukazatelem myši tak, jak se s ním pohybujete po obrazovce pomocí funkce čtečky <link xref="howto_mouse_review">zkoumání myší</link>.</p>
    <p>Výchozí hodnota: nezaškrtnuto</p>
  </section>
  <section id="timeanddate">
    <info>
      <desc>Volba v dialogovém okně s předvolbami, která umožňuje přizpůsobit si formát data a času používaný čtečkou <app>Orca</app>.</desc>
    </info>
    <title>Formát času a formát data</title>
    <p>Rozbalovací seznamy <gui>Formát času</gui> a <gui>Formát data</gui> umožňují určit, jak má <app>Orca</app> číst a předávat braillským zařízením čas a datum.</p>
    <p>Výchozí hodnota: u obou použít formát podle národního prostředí nastaveného v systému</p>
  </section>
  <section id="profiles">
    <info>
      <desc>Volby v dialogovém okně s předvolbami sloužící ke správě profilů s nastavením.</desc>
    </info>
    <title>Profily</title>
    <p>Skupina ovládacích prvků <gui>Profily</gui> se nachází v dolní části karty <gui>Obecné</gui> a umožňuje vám spravovat a používat vícero nastavení.</p>
   <list>
      <item>
        <p>Rozbalovací seznam <gui>Aktivní profil</gui> zobrazuje aktuální profil a umožňuje vám vybrat jiný, který se má načíst.</p>
      </item>
      <item>
        <p>Tlačítko <gui>Načíst</gui> způsobí, že <app>Orca</app> načte profil vybraný v rozbalovacím seznamu <gui>Aktivní profil</gui>.</p>
      </item>
      <item>
        <p>Tlačítko <gui>Uložit jako</gui> umožňuje uložit aktuální sadu voleb z dialogového okna předvoleb do pojmenovaného profilu.</p>
      </item>
      <item>
        <p>Rozbalovací seznam <gui>Počáteční profil</gui> vám umožňje vybrat si profil, který by se měl automaticky načíst pokaždé, když se <app>Orca</app> spustí.</p>
      </item>
    </list>
  </section>
</page>
