<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_speech" xml:lang="sl">
  <info>
    <link type="guide" xref="preferences#orca"/>
    <link type="next" xref="preferences_braille"/>
    <title type="sort">2.1 Speech</title>
    <title type="link">Govor</title>
    <desc>
      Configuring what gets spoken
    </desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons deljenje pod enakimi pogoji 3.0</p>
    </license>
  </info>
  <title>Možnosti govora</title>
  <section id="enable_speech">
    <title>Omogoči govor</title>
    <p>Izbirno polje <gui>Omogoči govor</gui> nadzira ali bo <app>Orka</app> uporabila sintetizator govora ali ne. Uporabniki, ki uporabljajo le povečavo ali Braillovo pisavo, bodo to verjetno to želeli onemogočiti.</p>
    <p>Privzeta vrednost: izbrano</p>
  </section>
  <section id="verbosity">
    <title>Razločnost</title>
    <p>Nastavitev <gui>Razločnost</gui> določi količino podatkov, ki je izgovorjena v določenih situacijah. Na primer, če je nastavljeno na podrobno, bo <app>Orka</app> izgovorila tipkovne bližnjice za predmete v menijih. Ko je nastavljena na kratko, tipkovne bližnjice niso oznanjene.</p>
    <p>Privzeta vrednost: <gui>Podrobno</gui></p>
  </section>
  <section id="table_rows">
    <title>Vrstice razpredelnice</title>
    <p>Skupina izbirnih gumbov <gui>Vrstice razpredelnice</gui> določi, kaj bo izgovorjeno ob krmarjenju v vrsticah v razpredelnici. Razpoložljive možnosti so <gui>govori vrstico</gui> in <gui>govori celico</gui>.</p>
    <p>Razmislite o opravilu preučevanja seznama sporočil vašem dohodnem predalu. Da lahko <app>Orka</app> oznani pošiljatelja, zadevo, datum in prisotnost prilog, bi morali <gui>izgovoriti vrstico</gui>. Po drugi strani pa pri krmarjenju med vrsticami, poslušanje celotne vrstice ni želeno. V tem primeru izberite <gui>izgovori celico</gui>.</p>
    <p>Privzeta vrednost: <gui>Izgovori vrstico</gui></p>
  </section>
  <section id="punctuation_level">
    <title>Raven ločil</title>
    <p>Skupino izbirnih gumbov <gui>Raven ločil</gui> lahko uporabite za prilagajanje količine ločil, ki jih izgovori sintetizator. Razpoložljive ravni so <gui>Brez</gui>, <gui>Nekaj</gui>, <gui>Večina</gui> in <gui>Vsa</gui>.</p>
    <p>Privzeta vrednost: <gui>Večina</gui></p>
    <section>
      <title>Brez</title>
      <p>Izbira ravni ločil <gui>Brez</gui> določi, da ni izgovorjeno nobeno ločilo. Vendar so na ti ravni še vedno izgovorjeni posebni simboli kot so podpisana in nadpisana števila, ulomki unicode in seznamske točke.</p>
    </section>
    <section>
      <title>Nekatera</title>
      <p>Izbira ravni ločil <gui>Nekaj</gui> določi izgovorjavo vseh predhodno omenjenih simbolov. Poleg tega <app>Orka</app> izgovori znane matematične simbole, simbole enot in  "^", "@", "/", "&amp;", "#".</p>
    </section>
    <section>
      <title>Večina</title>
      <p>Izbira ravni loči <gui>Večina</gui> izgovori vse predhodno omenjene simbole. Poleg tega bo <app>Orka</app> izgovorila vse druge simbole ločil <em>razen</em>  "!", "'", ",", ".", "?".</p>
    </section>
    <section>
      <title>Vsa</title>
      <p>Izbira ravni ločil <gui>Vse</gui> določi, da <app>Orka</app> izgovori vse znane simbole ločil.</p>
    </section>
  </section>
  <section id="spoken_context">
    <title>Spoken Context</title>
    <p>Naslednji predmeti nadzirajo predstavitev dodatnih, "sistemskih" podatkov o predmetu z žariščem. Ker se povezano besedilo ne pojavi na zaslonu, so ti podatki predstavljeni v <app>Orkinem</app> sistemskem zvoku.</p>
    <section id="only_speak_displayed_text">
      <title>Izgovori le prikazano besedilo</title>
      <p>Izbira izbirnega polja določi, da <app>Orka</app> izgovori le besedilo prikazano na zaslonu. Ta možnost je namenjena predvsem za slabovidne uporabnike in uporabnike z nezmožnostjo učenja vida.</p>
      <p>Prikazana vrednost: ni izbrano</p>
      <note style="note">
        <p>Naslednji predmeti ne bodo na voljo za nastavitev, če je izbirno polje <gui>Izgovori le prikazano besedilo</gui> izbrano.</p>
      </note>
    </section>
    <section id="speak_blank_lines">
      <title>Izgovorite prazne vrstice</title>
      <p>V primeru, da je izbirno polje <gui>Izgovori prazne vrstice</gui>, bo <app>Orka</app> rekla "prazno" vsakič, ko pridete do prazne vrstice. Če polje ni izbrano, <app>orka</app> ob premiku na prazno vrstico ne bo rekla ničesar.</p>
      <p>Privzeta vrednost: izbrano</p>
    </section>
    <section id="indentation_and_justification">
      <title>Izgovarite zamik in poravnavo</title>
      <p>Pri delu s kodo ali urejanju dokumentov je pogosto dobro vedeti, kje so poravnave in zamiki. Izbira izbirnega polja <gui>Izgovarjanje zamikov in poravnav</gui> bo povzročila, da bo <app>Orka</app> oznanila ta podatke.</p>
      <p>Prikazana vrednost: ni izbrano</p>
    </section>
    <section id="mnemonics">
     <title>Izgovorite mnemotehniko predmeta</title>
      <p>V primeru da je izbirno polje <gui>Izgovori mnemotehniko predmeta</gui> izbrano, bo <app>Orka</app> oznanila mnemotehniko, povezano s predmetom s pozornostjo (kot je <keyseq><key>Izmenjalka</key><key>O</key></keyseq> za gumb <gui>V redu</gui>). </p>
      <p>Prikazana vrednost: ni izbrano</p>
    </section>
    <section id="child_position">
      <title>Izgovorite položaj podrejenega predmeta</title>
      <p>Izbira izbirnega polja <gui>Izgovori položaj podrejenega predmeta</gui> določi, da <app>Orka</app> oznani položaj predmeta v žarišču v menijih, seznamih in drevesih (na primer "9 od 16").</p>
      <p>Prikazana vrednost: ni izbrano</p>
    </section>
    <section id="speak_tutorial_messages">
      <title>Izgovorite sporočila vodnika</title>
      <p>V primeru, da je izbirno polje <gui>Izgovori sporočila vodnika</gui> izbrano, bo <app>Orka</app> med pomikanjem med predmeti po vmesniku  zagotovila dodatne podrobnosti kot sta kako se sporazumevati s predmetom, ki je trenutno v žarišču.</p>
      <p>Prikazana vrednost: ni izbrano</p>
    </section>
  </section>
  <section id="progress_bar_updates">
    <title>Posodobitve kazalnika napredka</title>
    <section>
      <title>Omogočeno</title>
      <p>V primeru, da je izbirno polje <gui>Omogočeno</gui> izbrano, bo <app>Orka</app>  občasno oznanila stanje vrstic napredka.</p>
      <p>Privzeta vrednost: izbrano</p>
    </section>
    <section>
      <title>Frekvenca (sek):</title>
      <p>To vrtilno polje določi pogostost oznanjevanja.</p>
      <p>Privzeta vrednost: 10</p>
    </section>
    <section>
      <title>Omeji na</title>
      <p>To spustno polje vam omogoča nadzor katere vrstice napredka naj bodo predstavljene, če je bilo predstavljanje posodobitev vrstic napredka omogočeno. Izbire so <gui>Vse</gui>, <gui>Program</gui> in <gui>Okno</gui>.</p>
      <p>Izbira <gui>Vse</gui> določi, da <app>Orka</app> predstavi posodobitve za vse vrstice napredka ne glede na to kje so.</p>
      <p>Izbira <gui>Program</gui> določi, da <app>Orka</app> predstavi posodobitve iz vrstic napredka v dejavnem programu, tudi če niso v dejavnem oknu.</p>
      <p>Izbira <gui>Okno</gui> določi, da <app>Orka</app> predstavi le posodobitve vrstic napredka v dejavnem oknu.</p>
      <p>Privzeta vrednost: <gui>Program</gui></p>
    </section>
  </section>
</page>
