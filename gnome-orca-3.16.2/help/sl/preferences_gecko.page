<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_gecko" xml:lang="sl">
  <info>
    <link type="guide" xref="preferences#application"/>
    <link type="next" xref="preferences_table_navigation"/>
    <title type="sort">1. Krmarjenje Gecka</title>
    <title type="link">Krmarjenje Gecka</title>
    <desc>Nastavljanje <app>Orkine</app> podpore za <app>Firefox</app> in <app>Thunderbird</app>.</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons deljenje pod enakimi pogoji 3.0</p>
    </license>
  </info>
  <title>Možnosti krmarjenja Gecko</title>
  <section id="page_navigation">
    <title>Krmarjenje po strani</title>
    <p>Skupni nadzornikov <gui>Krmarjenje strani</gui> vam omogoča prilagajanje <app>Orkinega</app> prikaza in možnosti sporazumevanja z besedilom in drugo vsebino.</p>
    <section>
      <title>Control caret navigation</title>
      <p>To izbirno polje vklopi in izklopi <app>Okrino</app> krmarjenje kazalke. Ko je krmarjenje vklopljeno, <app>Orka</app> prevzame nadzor nad kazalko medtem ko se premikate po strani. Ko je krmarjenje izklopljeno je dejavna Geckovo lastno krmarjenje kazalke.</p>
      <p>Privzeta vrednost: izbrano</p>
      <note style="tip">
        <title>To nastavitev lahko preklapljate sproti</title>
        <p>Za sproten preklop te nastavitve brez shranjevanja uporabite <keyseq><key>Pomožna tipka Orka</key><key>F12</key></keyseq></p>
       </note>
    </section>
    <section>
      <title>Enable structural navigation</title>
      <p>To izbirno polje vklopi in izklopi <app>Orkino</app>'s <link xref="howto_structural_navigation">Strukturno krmarjenje</link>. Structural Navigation allows you to navigate by elements such as headings, links, and form fields.</p>
      <p>Privzeta vrednost: izbrano</p>
      <note style="tip">
        <title>To nastavitev lahko preklapljate sproti</title>
        <p>Za sproten preklop te nastavitve brez shranjevanja uporabite <keyseq><key>Pomožna tipka Orka</key><key>Z</key></keyseq></p>
       </note>
    </section>
    <section>
      <title>Postavi kazalko na začetek vrstice ob krmarjenju navpično</title>
      <p>V primeru, da je to izbirno polje izbrano, bo <app>Orka</app> v primeru, ko nadzira kazalko, le-to vedno premaknila kazalko na začetek vrstice. Predhodno in naslednjo vrstico lahko preberete s pritiskom na tipko Puščica navzgor ali Puščica navzdol. V primeru, da to izbirno polje ni izbrano, bo <app>Orka</app> poskusila kazalko obdržati na enakem vodoravnem položaju medtem ko krmarite navzgor ali navzdol.</p>
      <p>Privzeta vrednost: izbrano</p>
    </section>
    <section>
      <title>Automatically start speaking a page when it is first loaded</title>
      <p>
        If this checkbox is checked, <app>Orca</app> will perform a Say All on
        the newly opened web page or email.
      </p>
      <p>
        Default value: checked for Firefox; not checked for Thunderbird
      </p>
    </section>
  </section>
  <section id="table_options">
    <title>Table Options</title>
    <note>
      <p>
        To learn more about <app>Orca's</app> options for navigating within
        tables, please see <link xref="preferences_table_navigation">Table
	Navigation Preferences</link>.
      </p>
    </note>
  </section>
  <section id="find_options">
    <title>Možnosti iskanja</title>
    <p>Skupina nadzornikom <gui>Možnosti iksanja</gui> vam omogočajo prilagajanje <app>Orkine</app> predstavitve rezultatov iskanja z uporabo programove vgrajene zmožnosti iskanja.</p>
    <section>
      <title>Izgovori rezultate med iskanjem</title>
      <p>V primeru, da je izbrano to izbirno polje, bo <app>Orka</app> prebrala vrstico, ki se ujema z vašo poizvedbo iskanja.</p>
      <p>Privzeta vrednost: izbrano</p>
    </section>
    <section>
      <title>Izgovori le spremenjene vrstice med iskanjem</title>
      <p>V primeru da je to izbiro polje izbrano, <app>Orka</app> ne bo predstavila ujemajoče vrstice, če je enaka vrstica kot predhodni zadetek. Ta možnost je zasnovana za preprečevanje "klepetavosti" v vrstici z več primeri niza, ki ga iščete.</p>
      <p>Prikazana vrednost: ni izbrano</p>
    </section>
    <section>
      <title>Najmanjša dolžina ujemajočega se besedila</title>
      <p>V tem uredljivem vrtilnem gumbu lahko navedete število znakov, ki se morajo ujemati preden <app>Orka</app> oznani ujemajočo vrstico. Ta možnost je zasnovana za preprečevanje "klepetavosti" v vrstici z več primeri niza, ki ga iščete.</p>
      <p>Privzeta vrednost: 4</p>
    </section>
  </section>
</page>
