-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: gnome-orca
Binary: gnome-orca
Architecture: all
Version: 3.16.2-1
Maintainer: Debian Accessibility Team <debian-accessibility@lists.debian.org>
Uploaders: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>, Mario Lang <mlang@debian.org>, Michael Biebl <biebl@debian.org>
Homepage: https://wiki.gnome.org/Projects/Orca
Standards-Version: 3.9.6
Vcs-Browser: http://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/gnome-orca
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/gnome-orca
Build-Depends: cdbs (>= 0.4.90~), debhelper (>= 9), autotools-dev, gnome-pkg-tools (>= 0.10), intltool (>= 0.50.0), libatk-bridge2.0-dev (>= 2.10), libatspi2.0-dev (>= 2.10), pkg-config, python3 (>= 3.3), python-gi-dev (>= 3.10), python3-brlapi (>= 0.5.1), python3-louis (>= 1.6.2), liblouis-dev, python3-pyatspi (>= 2.10), python3-speechd (>= 0.8), yelp-tools
Package-List:
 gnome-orca deb gnome optional arch=all
Checksums-Sha1:
 f50b141cc1d8a9ccc7536367a4759d5b4412b150 2078728 gnome-orca_3.16.2.orig.tar.xz
 877ae4ef14a0f8d331acd886e2641426cdb58480 7924 gnome-orca_3.16.2-1.debian.tar.xz
Checksums-Sha256:
 cd14b28878cc04166ec43c7d9a8e6b0056c66cbfb1e4f934f12f74568937789a 2078728 gnome-orca_3.16.2.orig.tar.xz
 04e9ffe6504ef006b86a23e2ccd0b4dbd320f5de2b639f4148b512c525489ed2 7924 gnome-orca_3.16.2-1.debian.tar.xz
Files:
 5bc0c703ca8e1faff93c1cbf24055567 2078728 gnome-orca_3.16.2.orig.tar.xz
 c37b9795ce77b6ad68ce2a9ef5a66dbb 7924 gnome-orca_3.16.2-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQGVAwUBVV7njLh9mtxGggJBAQh5jQv/fz9oMY7hzxII7hNe789ZmLLv95MqVEfC
PoctNNyuHQ3rqbPdQWRmRcbLN2ZJ6YPPE6L4w2CTfRPzLqEoQICIh+iaIr3vmfpv
MrnY+5hTPCE47b6X/oOx4xtvGXYlTcrVaXLSsU9KvWj+g0Rotmkewxe8w0foo44Y
yL4nnozeY+A3u4FYHxdEU34/rUjLBK/d1gb5XunLXbfTzYy6ETN+gV13n001+D7g
0r88ud0oWrsbM/m9FoxPeNm2wTFSez6J7Kwh1sJ7rtM0TSg05F1gEEbI8+eGrk7+
nQH2s/s6bBaAUSosTTUp4dPts6+zHdJ8VICN1h/L5ZhQLU4jkdf/Zl5cxyLqfsqL
qQMMv+2eLbEYrhXZFjGZp/7LK0mEjeQ3ZDdIjKJzRkie90GxFqDCdJ8ox0MeFcN6
n6NSyCR6LGJbG49GxUKoCB/Hs3gHUztSVm7tCyPG7gfB10Lmam36WOYGtbByvbBK
EZNT1PZfNGCk7qUGB04rQODZwq+GV341
=jY3X
-----END PGP SIGNATURE-----
